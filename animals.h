///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.h
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo Kendal Oya<kendalo@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo 02_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

/// Define the maximum number of cats or dogs in our array-database
#define MAX_SPECIES (20)

/// Gender is appropriate for all animals in this database
// enum Gender // @todo fill this out from here...
enum Gender {MALE, FEMALE};
enum Breed {MAIN_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
enum fixed {YES, NO};
enum Color {BLACK, WHITE, GREEN, RED, BLUE, PINK, PURPLE};
/// Return a string for the name of the color
/// @todo this is for you to implement
char* colorName (enum Color color);
char* Fixed (enum fixed isFixed);
char* catBreed(enum Breed breed);
char* genderName(enum Gender gender);
