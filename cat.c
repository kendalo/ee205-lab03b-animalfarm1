///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author @todo Kendal Oya <@todo kendalo@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo 02_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdio.h>

#include "cat.h"
#include "animals.h"
// @todo declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it
struct Cat catDB[MAX_SPECIES];

/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
void addAliceTheCat(int i) {
   strcpy(catDB[i].name, "Alice");
   catDB[i].gender = FEMALE;
   catDB[i].breed = MAIN_COON;
   catDB[i].isFixed = YES;
   catDB[i].weight = 12.34;
   catDB[i].collar1color = BLACK;
   catDB[i].collar2color = RED;
   catDB[i].license = 12345;
}



/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) {
   printf("Cat name = [%s]\n", catDB[i].name); 
   printf("    gender = [%s]\n", genderName(catDB[i].gender));
   printf("    breed = [%s]\n", catBreed(catDB[i].breed));
   printf("    isFixed = [%s]\n", Fixed(catDB[i].isFixed));
   printf("    weight = [%f]\n", catDB[i].weight);
   printf("    collar color 1 = [%s]\n", colorName(catDB[i].collar1color));
   printf("    collar color 2 = [%s]\n", colorName(catDB[i].collar2color));
   printf("    license = [%ld]\n",catDB[i].license);
   }


