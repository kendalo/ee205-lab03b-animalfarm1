###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author @todo Kendal Oya <@todo kendalo@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   @todo 02_Feb_2021
###############################################################################

TARGET = animalfarm
all: $(TARGET)
CC = gcc
CFLAGS = -g -Wall 


main.o: animals.c animals.h main.c cat.c cat.h
	$(CC) $(CFLAGS) -c main.c

cat.o: cat.c cat.h animals.h
	$(CC) $(CFLAGS) -c cat.c

animals.o: animals.h animals.c cat.h
	$(CC) $(CFLAGS) -c animals.c

animalfarm: main.o animals.o cat.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o cat.o animals.o


clean:
	rm -f *.o animalfarm
